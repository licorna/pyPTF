"""Base implementation of predictor with uncertainty assessment."""

import numpy as np


def mahalanobis(data, centroids, W):
    """Calculate the mahalanobis distance between observations and centroids.

    Parameters
    ----------
    data : np.ndarray
        2-dimensional array containing the observations.
    centroids : np.ndarray
        2-dimensional `centroids` array. It should have the same number of columns
        than `data`.
    W : np.ndarray
        The inverse of the covariance matrix.

    Returns
    -------
    np.ndarray
        Distance matrix.

    """
    dist = []
    for c in centroids:
        c_rep = np.repeat([c], data.shape[0], 0)
        Dc = c_rep - data
        dist.append((np.matmul(Dc, W) * Dc).sum(1))
    return np.transpose(dist)


def euclidean(data, centroids):
    """Calculate the euclidean distance between observations and centroids.

    Parameters
    ----------
    data : np.ndarray
        2-dimensional array containing the observations.
    centroids : np.ndarray
        2-dimensional `centroids` array. It should have the same number of columns
        than `data`.

    Returns
    -------
    np.ndarray
        Distance matrix.

    """
    return np.sqrt(np.power([data - np.repeat([c], data.shape[0], 0) for c in centroids], 2).sum(2)).T


def _calc_dist(data, centroids, W, disttype):
    if (disttype == 'euclidean'):
        dist = euclidean(data, centroids)
    else:  # Diagonal and Mahalanobis
        dist = mahalanobis(data, centroids, W)
    return dist


def _calc_membership(dist, phi, a1):
    nclass = dist.shape[1]
    tmp = dist ** (-2 / (phi - 1))
    tm2 = dist ** (-2)
    s2 = (a1 * tm2.sum(1)) ** (-1 / (phi - 1))

    t1 = tmp.sum(1)
    t2 = np.repeat([t1], nclass, 0).T + np.repeat([s2], nclass, 0).T
    U = tmp / t2
    Ue = 1 - U.sum(1)
    return np.abs(U), np.abs(Ue)


class FKMExPredictor(object):
    """Fuzzy k-means with extragrades predictor.

    This class has the minimum requirements to be able to assign the membership
    of a point to the the different classes. To obtain its parameters, use
    :class:`~.fkmeans.FKMEx`.

    Parameters
    ----------
    centroids : np.ndarray
        Coordinates of the centroids.
    W : np.ndarray
        The inverse of the data covariance matrix.
    phi : float
        Degree of fuzziness or overlap of the generated clusters.
        Usually in the range [1, 2].
    alpha : float
        Mean membership of the extragrade class (the default is None).
        This parameter is usually obtained by optimisation. See :func:`FKMEx.fit`.
        Once obtained by optimisation, it can be provided to skip the optimisation.
    disttype : {'euclidean', 'diagonal', 'mahalanobis'}
        Type of distance (the default is 'mahalanobis').

    """
    def __init__(self, centroids, W, phi, alpha, disttype='mahalanobis'):
        self.alpha = alpha
        self.centroids = centroids
        self.W = W
        self.phi = phi
        self.disttype = disttype

    @property
    def fitted(self):
        return ((self.centroids is not None) &
                (self.W is not None) &
                (self.phi is not None) &
                (self.alpha is not None))

    def dist(self, data):
        """Calculate the distance between data and the class centroids.

        Parameters
        ----------
        data : np.ndarray
            2-dimensional array containing the observations.

        Returns
        -------
        np.ndarray
            Distance matrix.

        """
        if self.fitted:
            return _calc_dist(data, self.centroids, self.W, self.disttype)

    def membership(self, data):
        """Calculate the membership to each class.

        Parameters
        ----------
        data : np.ndarray
            2-dimensional array containing the observations.

        Returns
        -------
        np.ndarray
            Memebership matrix.

        """
        if self.fitted:
            dist = self.dist(data)
            a1 = (1 - self.alpha) / self.alpha
            U, Ue = _calc_membership(dist, self.phi, a1)
            return np.concatenate([U, Ue.reshape(-1, 1)], 1)

